

var memory_array = ['meme1.jpg', 'meme1.jpg', 'meme2.jpg', 'meme2.jpg', 'meme3.jpg', 'meme3.jpg', 'meme4.jpg', 'meme4.jpg', 'meme5.jpg', 'meme5.jpg', 'meme6.jpg', 'meme6.jpg', 'meme7.jpg', 'meme7.jpg', 'meme8.jpg', 'meme8.jpg', 'meme9.jpg', 'meme9.jpg', 'meme10.jpg', 'meme10.jpg', 'meme11.jpg', 'meme11.jpg', 'meme12.jpg', 'meme12.jpg'];
var memory_values = []; 
var memory_tile_ids = [];
var tiles_flipped = 0; 
Array.prototype.memory_tile_shuffle = function () {   
    var a = this.length, i, temp;
    while (a-- > 0) {
        i = Math.floor(Math.random() * (a + 1)); 
        temp = this[i];
        this[i] = this[a];
        this[a] = temp;
    }
}

function newBoard() { 
    tiles_flipped = 0;
    var output = '';
    memory_array.memory_tile_shuffle(); 
    for (var x = 0; x < memory_array.length; x++) { 
        output += '<div id = "tile_' + x + '" onclick="memoryFlipTile(this,\''+ memory_array[x] +'\')"></div>'; 
    }
    document.getElementById('memory_board').innerHTML = output; 
}
function memoryFlipTile(tile, val) { 
    if (tile.innerHTML == "" && memory_values.length < 2) { 
        tile.innerHTML = '<img src="' + val + '">';
        if (memory_values.length == 0) { 
            memory_values.push(val);
            memory_tile_ids.push(tile.id);
        } else if (memory_values.length == 1) { 
            memory_values.push(val);
            memory_tile_ids.push(tile.id);
            if (memory_values[1] == memory_values[0]) { 
                tiles_flipped += 2;
                //clear both arrays
                memory_values = [];
                memory_tile_ids = [];
                //Check to see if the whole bord is cleared
                if (tiles_flipped == memory_array.length) {
                    alert("Congrats you finished the test... generating new board");
                    document.getElementById('memory_board'), innerHTML = "";
                    newBoard(); 
                }
            } else {
                function flip2Back() { // 
                    //Flip the 2 tiles back overflow
                    var tile_1 = document.getElementById(memory_tile_ids[0]); 
                    var tile_2 = document.getElementById(memory_tile_ids[1]); 
                    
                
                    tile_1.innerHTML = "";
             
                    tile_2.innerHTML = "";
                    //clear both arrays
                    memory_values = [];
                    memory_tile_ids = [];
                }
                setTimeout(flip2Back, 500); 
            }
        }
    }
}